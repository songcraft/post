import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-blog-post",
  templateUrl: "./blog-post.component.html",
  styleUrls: ["./blog-post.component.scss"]
})
export class BlogPostComponent implements OnInit {
  likeStatus: number = 0;
  postTitle: string = "Titre du post";
  lastUpdate = new Date();

  constructor() {}

  onLike() {
    console.log("Je like !");
    this.likeStatus += 1;
    console.log(this.likeStatus)
  }

  onDislike() {
    console.log("Je dislike !");
    this.likeStatus -= 1;
    console.log(this.likeStatus)
  }

  getColor() {
    if(this.likeStatus > 0) {
      return 'green';
    } else if(this.likeStatus < 0) {
      return 'red';
    }
}
  ngOnInit() {}
}
